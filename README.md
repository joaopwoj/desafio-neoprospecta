# GeneticAPI

Coding challenge by Neoprospecta to the backend opportunity.
    

## Dependencies

This project requires the following dependencies and libraries:

- `python` >=3.7.3
    - `django` >= 2.1.7
    - `djangorestframework` >= 3.9.2
    - `django-tables2` >= 2.0.6
    - `django-filter` >= 2.1.0

## Setting up

First create and activate a `virtualenv` on the root of this repo by executing

```
$ virtualenv venv
$ source ./venv/bin/activate
```

then install the necessary dependencies provided in the `requirements.txt` file

    $ pip install -r requirements.txt

You may want create a superuser to access the admin portion of django by executing

    $ python manage.py createsuperuser

## Populating the database

The command `fetchdb` is provided with this server. This command fetches a FASTA formatted genetic database from a given `--url` or `--filename`. 

It automatically detects if the file is gzipped, uncrompresses it, parses the data and properly populates the model databases. By default, it skips if 
records that already exists, but it may be overwritten if the `--update` flag is given.

For example, to download only the first 50 entries from a given url, run

    $ python manage.py fetchdb -n 50 --url http://path/to/db.gz

Below is the relevant usage bits you need to know.

```
usage: manage.py fetchdb [-h] (--url [URL] | --filename [FILENAME])
                         [--n_entries [N_ENTRIES]] [--update] 

Fetches a database from a given url or file.

optional arguments:
  -h, --help            show this help message and exit
  --url [URL], -u [URL]
                        Download a FASTA gzipped database from the given url.
  --filename [FILENAME], -f [FILENAME]
                        Read a given FASTA database. Accepts gzip or fasta
                        files.
  --n_entries [N_ENTRIES], -n [N_ENTRIES]
                        Number of entries to import.
  --update              Update records if exist.
```
## Running the server up

To put the server online, you only need to execute

    $ django-admin startproject GeneticAPI

which defaults to http://127.0.0.1:8080/.

The configured endpoints are:

- http://127.0.0.1:8080/admin
- http://127.0.0.1:8080/table/
- http://127.0.0.1:8080/entry/
    - With filters:
        - ?access_id=
        - ?kingdom__name=
        - ?specie__name=
- http://127.0.0.1:8080/entry/<+id+>