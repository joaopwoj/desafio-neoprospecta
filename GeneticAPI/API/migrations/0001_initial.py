# Generated by Django 2.2 on 2019-04-27 14:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Kingdom',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Specie',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('access_id', models.CharField(max_length=32, unique=True)),
                ('sequence', models.CharField(max_length=100)),
                ('kingdom', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='kingdom', to='API.Kingdom')),
                ('specie', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='specie', to='API.Specie')),
            ],
        ),
    ]
