from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'entry', views.EntryViewset)

urlpatterns = [
    path('', include(router.urls)),
    path('table/', views.EntryTable.as_view())
]