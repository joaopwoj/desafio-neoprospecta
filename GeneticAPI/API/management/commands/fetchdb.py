from django.core.management.base import BaseCommand, CommandError
from API.serializers import EntrySerializer
from API.models import Entry

class Command(BaseCommand):
    help = 'Fetches a database from a given url or file.'

    def add_arguments(self, parser):
        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument('--url','-u', nargs='?', type=str, 
                           help='Download a FASTA gzipped database from the given url.')
        group.add_argument('--filename','-f', nargs='?', type=str, 
                           help='Read a given FASTA database. Accepts gzip or fasta files.')
        parser.add_argument('--n_entries','-n', nargs='?', type=int, default=5000,
                            help='Number of entries to import.')
        parser.add_argument('--update', action='store_true', default=False,
                            help='Update records if exist.')

    def handle(self, *args, **options):
        n_entries = options['n_entries']
        data = self._fetchGeneticEntries(url=options['url'],
                                         filename=options['filename'],
                                         n_entries=n_entries)
        for i, entry in enumerate(data):
            self.stdout.write("[{}/{}] Creating record {}..."
                                .format(i+1, n_entries, entry['access_id']),
                                ending='')
            self._import(entry, options['update'])

    def _import(self, data: dict, update: bool=False):
        if not update:
            query = Entry.objects.filter(access_id = data['access_id'])
            if query.count() != 0:
                self.stdout.write(self.style.WARNING('EXISTS, SKIPPING!'))
                return

        s = EntrySerializer(data=data)
        if s.is_valid():
            s.save()
            self.stdout.write(self.style.SUCCESS('DONE!'))
        else:
            self.stdout.write(self.style.ERROR('FAIL!'))

    def _downloadFASTA(self, url: str) -> str:
        from urllib import request
        from urllib.error import HTTPError
        import gzip
        try:
            response = request.urlopen(url)
            if response.headers.get_content_type() != 'application/gzip':
                self.stderr.write(self.style.ERROR("File is expected to be gzipped. Please provide a valid URL."))
            
            self.stdout.write("Downloading {}...".format(url))
            data = gzip.decompress(response.read())
            data = str(data, 'utf-8')
            return data

        except HTTPError:
            self.stderr.write(self.style.ERROR("URL not found. Please provide a valid URL."))

    def _loadFASTA(self, filename: str) -> str:
        if filename.endswith('.gz'):
            with open(filename, 'rb') as f:
                import gzip
                data = gzip.open(f).read()
                data = str(data,'utf-8')
                return data
        else:
            with open(filename, 'r') as f:
                data = f.read()
                return data

    def _zipGeneticEntry(self, entry: str, returnEverything: bool=False) -> dict:
        data = entry.split('\n')
        header = data[0]
        id = header.split(' ')[0]
        taxonomy = ' '.join(header.split(' ')[1:]).split(';')
        sequence = data[1:-1]
        if returnEverything:
            return {
                'access_id': id,
                'taxonomy': taxonomy,
                'sequence': sequence
            }
        else:
            return {
                'access_id': id,
                'kingdom': {'name': taxonomy[0]},
                'specie': {'name': taxonomy[-1]},
                'sequence': sequence[0]
            }

    def _fetchGeneticEntries(self, n_entries: int = 5000,
                                   url: str = None,
                                   filename: str=None) -> list:
        if filename is not None:
            data = self._loadFASTA(filename)
        elif url is not None:
            data = self._downloadFASTA(url)
        else:
            self.stderr.write(self.style.ERROR('\'url\' or \'filename\' is empty'))

        # Split returns the 0th element empty
        data = data.split('>')[1:]
        totalEntries = len(data)

        # Clamp the given n_entries to the maximum amount
        if n_entries > totalEntries: n_entries = totalEntries
        
        return list(map(self._zipGeneticEntry,data[:n_entries]))
