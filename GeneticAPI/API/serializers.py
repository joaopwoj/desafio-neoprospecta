from rest_framework import serializers
from .models import Entry, Kingdom, Specie
   
class SpecieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Specie
        fields = ('name',)

class KingdomSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Kingdom
        fields = ('name',)

class EntrySerializer(serializers.HyperlinkedModelSerializer):
    specie = SpecieSerializer()
    kingdom = KingdomSerializer()

    class Meta:
        model = Entry
        fields = ('url', 'id','access_id', 'sequence', 'kingdom', 'specie')
        extra_kwargs = {
            'access_id': {'validators': []},
        }

    def create(self, data):
        specie,_ = Specie.objects.get_or_create(name=data['specie']['name'])
        kingdom,_ = Kingdom.objects.get_or_create(name=data['kingdom']['name'])
        return Entry.objects.update_or_create(access_id=data['access_id'],
                     sequence=data['sequence'],
                     specie=specie,
                     kingdom=kingdom)

    def update(self, instance, data):
        specie,_ = Specie.objects.get_or_create(name=data['specie']['name'])
        kingdom,_ = Kingdom.objects.get_or_create(name=data['kingdom']['name'])

        instance.specie = specie
        instance.kingdom = kingdom
        instance.sequence = data['sequence']
        instance.access_id = data['access_id']
        instance.save()

        return instance
