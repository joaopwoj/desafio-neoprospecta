from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend 

import django_tables2 as tables

from .models import Entry
from .serializers import EntrySerializer

class EntryViewset(viewsets.ModelViewSet):
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ('kingdom__name', 'access_id', 'specie__name')

    def perform_create(self, serializer):
        serializer.save()

class EntryTable(tables.SingleTableView):
    model = Entry