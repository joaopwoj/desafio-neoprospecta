from django.contrib import admin

# Register your models here.
from .models import Kingdom, Specie, Entry

admin.site.register(Specie)
admin.site.register(Kingdom)
admin.site.register(Entry)