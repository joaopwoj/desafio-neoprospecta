from django.db import models

# Create your models here.
class Kingdom(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name


class Specie(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Entry(models.Model):
    id = models.AutoField(primary_key=True)
    access_id = models.CharField(unique=True, max_length=32)
    kingdom = models.ForeignKey(Kingdom, related_name='kingdom', on_delete=models.CASCADE)
    specie = models.ForeignKey(Specie, related_name='specie', on_delete=models.CASCADE)
    sequence = models.CharField(max_length=100)

    def __str__(self):
        return self.access_id
